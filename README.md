# WeTransfer Spinner Challenge

### Demo
https://wetransfer-loader.web.app/

### Description
To give more context to the `<Spinner />`, I decided to buid a wrapper component called `<Box />` that is a mock-up of the real one in production. The stack used was <b>React & Redux</b>. The tests were written using <b>Jest and Enzyme</b>. 

#### Spinner Reducer
The Spinner owns a reducer call spinner which has the following data

|Attribute | Type | Purpose  |
|- | - | -|
inProcess | `bool`|  Detemines the state of the `<Spinner />`
didSucceed | `bool` | Transaction completed 
percentage | `number` | Transaction progress
width | `number` | As we want to create a circle this number is the width and height of the spinner. It makes the spinner reusable since we can assign a value for example depending on the viewport clientX.
speed | `number` | `setInterval` delay parameter 
fileSize | `number` |  File size in MB - `Mock`


#### API 
|Function Name | Payload | Purpose  |
| - | - | - |
 `toggleSpinnerButton()`  | `action.type` | It starts or stops the spinner 
`updateSpinnerPercentage(prevPercentage : number)` | `action.type` `action.percentage` |  It updates the percentage adding 1 unit to the previous percentage
`finishedSpinnerTransaction()` | `action.type` | It shows that the transaction succeeed. No payload 
`resetSpinner()` | `action.type` | It resets the spinner


The `<Spinner />` contains a condition that renders the `<SpinnerCircle />` and `<SpinnerPercentage />` in case the variable `didSucceed` is false. Otherwise it renders `<SpinnerCircle />`and `<GreenTick />` showing a successful transaction.
In the first case each component is surrounded by an `<ErrorBoundary/>`. The reason is because in case the circles or percentage <i>breaks</i> we want to show the user that the transferring is still in process.

### The magic loader
Here I explain the recipe on how I came up with the loader that grows as the progress increments while it is rotating. I used SVG circles to get the desired result manipulating its `stroke-dasharray` and `stroke-dashoffset`attributes

1. Create the `<svg />` tag
2. We get the value of <b>width</b> from the spinner reducer
3. Assign the value to the viewBox attribute `0 0 ${width} ${width}`
4. Calculate the <b>center</b> of the svg tag  `width / 2`
5. Calculate the <b>radius</b> of the circle  `(width - strokeWidth) /2 `
6. Calculate the <b>circumference</b> `2 * π * radius` 
7. Calculate the <b>offset</b> 
7. Place the <b>circumference</b> in `strokeDasharray` to create just <b>1 dash</b> (It'll be the 100%)
8. Now If we place <b>circumference</b> in `strokeDashoffset`, we won't see the circle since we change the starting point to the gap between the dashes which length is the circumference.
9. To show the `100%`  we need to gradually reduce the offset to cero  starting from the value of `circumference` 
10. Now we need to fix the loader since it starts with an offset. We'll add `transform: rotate(-90deg)`
11. Finally to make the loader rotate we need to create an animation which has to start with `transform: rotate(-90deg)` and finish with `transform : rotate(275deg)`
12. While the animation is running, if we need to stop the loader in its current position we have to add `animation-play-state: paused`
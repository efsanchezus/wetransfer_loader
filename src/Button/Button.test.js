import React from 'react'
import { shallow, mount } from 'enzyme'
import renderer from 'react-test-renderer'
import { Button } from './Button'

describe('Button', () => {
	let wrapper, props;

	beforeEach(() => {
		props = {
			message : "",
			onClick : jest.fn()
		}
		
		wrapper = shallow(<Button {...props}/>)
	})

	it('should render correctly', () => {
		const component = renderer.create(<Button {...props}/>)
		let tree = component.toJSON()
		expect(tree).toMatchSnapshot()
	})

	it('should render a <button />', () => {
		expect(wrapper.find('button').length).toEqual(1)
	})
	it('renders the value of message', () => {
		wrapper.setProps({ message : "Start again" })
		expect(wrapper.text()).toEqual('Start again')
		wrapper.setProps({ message : "Start" })
		expect(wrapper.text()).toEqual('Start')
		wrapper.setProps({ message : "Stop" })
		expect(wrapper.text()).toEqual('Stop')
	})	


})

describe('mounted Button', () => {
	let wrapper;
	const mockFunction = jest.fn();

	beforeEach(() => wrapper = mount(<Button onClick={mockFunction} message=""/>))

	it('calls onClick when button is clicked', () => {
		  wrapper.find('button').simulate('click');
		  expect(mockFunction).toHaveBeenCalled();
	})
})
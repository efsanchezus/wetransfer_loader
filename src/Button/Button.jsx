import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './Button.css' 

export function Button({ onClick, message }){
	return <button className='Button' onClick={onClick}>{ message }</button>
}

const mapStateToProps = ({ spinner }) => {
	const { inProcess, didSucceed } = spinner
	let message = ""

	if(didSucceed)
		message = "Start again"
	else if(inProcess)
		message = "Stop"
	else
		message = "Start"

	return{ message }
}

Button.propTypes = {
	onClick : PropTypes.func.isRequired,
	message : PropTypes.string.isRequired
}

export default connect(mapStateToProps)(Button)
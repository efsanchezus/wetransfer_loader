import React from 'react'
import { Provider } from 'react-redux'
import Box from '../Box/Box'
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'
import PropTypes from 'prop-types'
import './App.css'

const App = ({ store }) => (
	<Provider store={store}>
		<div className='App'>
			<ErrorBoundary text="Something went wrong">
				<Box />
			</ErrorBoundary>
		</div>
	</Provider>
)

App.propTypes = {
	store : PropTypes.object.isRequired
}
export  default App
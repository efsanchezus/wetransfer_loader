import React from 'react'
import { shallow } from 'enzyme'
import App from './App'
import Box from '../Box/Box'
import createMockStore from 'redux-mock-store'
const mock = createMockStore()

describe('App', () => {
	let wrapper, store;
	beforeEach(() => {
		store = mock({})
		wrapper = shallow(
			<App store={store}/>
		)
	})

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})
	it('should render the Box Component', () => {
		expect(wrapper.containsMatchingElement(<Box />)).toEqual(true)
	})
})
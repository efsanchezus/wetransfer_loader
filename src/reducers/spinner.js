import { 
	TOGGLE_SPINNER_BUTTON,
	UPDATE_PERCENTAGE,
	FINISHED_SPINNER_TRANSACTION,
	RESET_SPINNER
} from '../actions'

const initialState = {
	inProcess : false,
	didSucceed : false,
	percentage : 0,
	width : 120,
	speed : 50,
	fileSize : 1000	
}

const spinner = (state = initialState, action) => {
	switch(action.type){
		case TOGGLE_SPINNER_BUTTON:
			return{
				...state,
				inProcess : !state.inProcess
			}		
		case RESET_SPINNER:
			return{
				...state,
				didSucceed : false,
				percentage : 0
			}
		case UPDATE_PERCENTAGE:
			return{
				...state,
				percentage : action.percentage > 100 ? 100 : action.percentage
			}
		case FINISHED_SPINNER_TRANSACTION:
			return{
				...state,
				didSucceed : true,
				inProcess : false
			}
		default:
			return state
	}
}

export default spinner
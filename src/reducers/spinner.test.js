import React from 'react'
import * as actions from '../actions/index'
import reducer from './spinner'

describe('spinner reducer', () => {
  	it('should return the initial state', () => {
    	expect(reducer(undefined, {})).toEqual(
      		{
				inProcess : false,
				didSucceed : false,
				percentage : 0,
				width : 120,
				speed : 50,
				fileSize : 1000	
      		}
    	)
  	})

  	it('should handle TOGGLE_SPINNER_BUTTON', () => {
	    expect( 
	    	reducer({}, { type: actions.TOGGLE_SPINNER_BUTTON })
	    ).toEqual({ inProcess : true })

	    expect( 
	    	reducer({ inProcess : true }, { type: actions.TOGGLE_SPINNER_BUTTON })
	    ).toEqual({ inProcess : false })
  	})

  	it('should handle UPDATE_PERCENTAGE', () => {
	    expect( 
	    	reducer({}, { type: actions.UPDATE_PERCENTAGE, percentage : 1  })
	    ).toEqual({ percentage : 1 })
	    expect( 
	    	reducer({ percentage : 1 }, { type: actions.UPDATE_PERCENTAGE, percentage : 20  })
	    ).toEqual({ percentage : 20 })
	    expect( 
	    	reducer({ percentage : 100 }, { type: actions.UPDATE_PERCENTAGE, percentage : 101  })
	    ).toEqual({ percentage : 100 })
  	})

  	it('should handle FINISHED_SPINNER_TRANSACTION', () => {
	    expect( 
	    	reducer({
				inProcess : false,
				didSucceed : false,	    		
	    	}, { type: actions.FINISHED_SPINNER_TRANSACTION })
	    ).toEqual({ 				
	    	didSucceed : true,
			inProcess : false 
		})

  	}) 

  	it('should handle RESET_SPINNER', () => {
	    expect( 
	    	reducer({
				didSucceed : false,
				percentage : 0	    		
	    	}, { type: actions.RESET_SPINNER })
	    ).toEqual({ 
			didSucceed : false,
			percentage : 0
	    })
  	})  	
})

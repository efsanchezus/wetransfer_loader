import React from 'react'
import { shallow } from 'enzyme'
import GreenTick from './GreenTick'

describe('GreenTick', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<GreenTick />))

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})
	it('should contain a <span />', () => {
		expect(wrapper.find('span').length).toEqual(1)
	})
})
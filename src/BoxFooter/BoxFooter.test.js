import React from 'react'
import { toggleSpinnerButton } from '../actions'
import { shallow } from 'enzyme'
import { BoxFooter } from './BoxFooter'
import Button from '../Button/Button'

import configureMockStore  from 'redux-mock-store';
const mockStore = configureMockStore();

describe('BoxFooter', () => {
	let wrapper, store;
	beforeEach(() => {
		store = mockStore({})
		wrapper = shallow(<BoxFooter store={store} />)
	})

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})
	it('should render the Button component', () => {
		expect(wrapper.containsMatchingElement(<Button />)).toEqual(true)
	})
    it('should call onClick when the button is clicked', () => {
        store.dispatch(toggleSpinnerButton())
        const actions = store.getActions()
        const expectedPayload = { type: 'TOGGLE_SPINNER_BUTTON' }
        expect(actions).toEqual([expectedPayload])
    });	
})

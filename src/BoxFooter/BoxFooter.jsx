import React from 'react'
import PropsType from 'prop-types'
import Button from '../Button/Button'
import { clickButton, updateSpinnerPercentage, finishedSpinnerTransaction, resetSpinner, toggleSpinnerButton } from '../actions/'
import { connect } from 'react-redux'
import './BoxFooter.css' 

export class BoxFooter extends React.PureComponent{
	constructor(){
		super()
		this.handleClick = this.handleClick.bind(this)
	}
	handleClick(){
		const { toggleSpinnerButton, inProcess, speed, didSucceed, resetSpinner } = this.props
		
		if(didSucceed) resetSpinner()
	
		toggleSpinnerButton()

		if(inProcess === false)
			this.intervalID = setInterval(() => this.tick(), speed)
		else
			clearInterval(this.intervalID)

	}
	tick(){
		const { updateSpinnerPercentage, finishedSpinnerTransaction, percentage } = this.props

		if(percentage === 100){
			finishedSpinnerTransaction()
			clearInterval(this.intervalID)
		}else{
			updateSpinnerPercentage(percentage)
		}
	}	
	componentWillUnmount(){
		clearInterval(this.intervalID)
	}
	render(){
		return(
			<div className='BoxFooter'>
				<Button onClick={this.handleClick}/>
			</div>
		)
	}
}

const mapStateToProps = ({ spinner }) => ({
	inProcess : spinner.inProcess,
	percentage : spinner.percentage,
	speed : spinner.speed,
	didSucceed : spinner.didSucceed,
})
const mapDispatchToProps = dispatch => ({
	toggleSpinnerButton : () => dispatch(toggleSpinnerButton()),
	updateSpinnerPercentage : (percentage) => dispatch(updateSpinnerPercentage(percentage)),
	finishedSpinnerTransaction : () => dispatch(finishedSpinnerTransaction()),
	resetSpinner : () => dispatch(resetSpinner())
})

BoxFooter.propsType = {
	toggleSpinnerButton : PropsType.func.isRequired,
	updateSpinnerPercentage : PropsType.func.isRequired,
	finishedSpinnerTransaction : PropsType.func.isRequired,
	resetSpinner : PropsType.func.isRequired,
	inProcess : PropsType.bool.isRequired,
	percentage : PropsType.number.isRequired,
	speed : PropsType.number.isRequired,
	didSucceed : PropsType.bool.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(BoxFooter)
import React from 'react'
import { shallow } from 'enzyme'
import ErrorBoundary from './ErrorBoundary'

describe('ErrorBoundary', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallow(<ErrorBoundary />)
	})

	it('should render a <p /> if there is an error', () => {
		wrapper.setState({ hasError : true })
		expect(wrapper.find('p').length).toEqual(1)
	})
})
import * as actions from './index'

describe('actions', () => {
  it('should create an action to toggle a button', () => {
    const expectedAction = {
      type: actions.TOGGLE_SPINNER_BUTTON
    }
    expect(actions.toggleSpinnerButton()).toEqual(expectedAction)
  })

  it('should create an action to update the percentage', () => {
    const expectedAction = {
      type: actions.UPDATE_PERCENTAGE,
      percentage : 11
    }
    expect(actions.updateSpinnerPercentage(10)).toEqual(expectedAction)
  })
  it('should create an action to update the percentage, next percentage should be equal or less than 100', () => {
    const expectedAction = {
      type: actions.UPDATE_PERCENTAGE,
      percentage : 100 
    }
    expect(actions.updateSpinnerPercentage(103)).toEqual(expectedAction)
  })

  it('should create an action to show the transaction is finished', () => {
    const expectedAction = {
      type: actions.FINISHED_SPINNER_TRANSACTION
    }
    expect(actions.finishedSpinnerTransaction()).toEqual(expectedAction)
  })

  it('should create an action to reset the transaction', () => {
    const expectedAction = {
      type: actions.RESET_SPINNER
    }
    expect(actions.resetSpinner()).toEqual(expectedAction)
  })

})
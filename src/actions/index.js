export const TOGGLE_SPINNER_BUTTON = "TOGGLE_SPINNER_BUTTON"
export const toggleSpinnerButton = () => ({ type : TOGGLE_SPINNER_BUTTON })

export const UPDATE_PERCENTAGE = "UPDATE_PERCENTAGE"
export const updateSpinnerPercentage = (prevPercentage) => {
	if(prevPercentage >= 100)
		prevPercentage = 100
	else
		prevPercentage += 1

	return{ 
		type : UPDATE_PERCENTAGE,
		percentage : prevPercentage
	}
}

export const FINISHED_SPINNER_TRANSACTION = "FINISHED_SPINNER_TRANSACTION"
export const finishedSpinnerTransaction = () => ({ type : FINISHED_SPINNER_TRANSACTION })

export const RESET_SPINNER = "RESET_SPINNER"
export const resetSpinner = () => ({ type : RESET_SPINNER })


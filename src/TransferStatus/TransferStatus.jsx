import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './TransferStatus.css'

export function TransferStatus({message}){
	return(<p className='TransferStatus'>{ message }</p>)
}

const mapStateToProps = ({ spinner }) => {
	const { didSucceed, inProcess } = spinner
	let message = ""
	
	if(didSucceed === true)
		message = "Transfer complete"
	else if(inProcess === true)
		message = "Transferring..."
	else
		message = "Transfer"

	return { message }
}
TransferStatus.propTypes = {
	message : PropTypes.string.isRequired
}
export default connect(mapStateToProps)(TransferStatus)
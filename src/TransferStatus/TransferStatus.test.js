import React from 'react'
import { shallow } from 'enzyme'
import { TransferStatus } from './TransferStatus'

describe('TransferStatus', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<TransferStatus message="" />))

	it('should render a <p />', () => {
		expect(wrapper.find('p').length).toEqual(1)
	})

	it('should render a message', () => {
		wrapper.setProps({ message : "test" })
		expect(wrapper.find('p').text()).toEqual("test")
	})
})

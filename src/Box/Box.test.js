import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import Box from './Box'
import BoxContent from '../BoxContent/BoxContent'
import BoxFooter from '../BoxFooter/BoxFooter'

describe('Box', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<Box />))

	/*
	it('should render correctly', () => {
		const component = renderer.create(<Box />)
		let tree = component.toJSON()
		expect(tree).toMatchSnapshot()
	})*/

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})

	it('should render the BoxContent and BoxFooter', () => {
		expect(wrapper.containsAllMatchingElements([
			<BoxContent />,
			<BoxFooter />,
		])).toEqual(true)
	})
})
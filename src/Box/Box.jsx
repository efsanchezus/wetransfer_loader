import React from 'react'
import BoxContent from '../BoxContent/BoxContent'
import BoxFooter from '../BoxFooter/BoxFooter'
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'
import './Box.css'

function Box(){
	return(
		<div className='Box'>
			<ErrorBoundary text="Something went wrong">
				<BoxContent />
			</ErrorBoundary>
			<ErrorBoundary text="Something went wrong">
				<BoxFooter />
			</ErrorBoundary>
		</div>
	)
}
export default Box
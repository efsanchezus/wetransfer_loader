import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './SpinnerPercentage.css' 

export class SpinnerPercentage extends React.PureComponent{
	render(){
		let { percentage } = this.props
		if(percentage > 100)
			percentage = 100
		return(
			<div className='SpinnerPercentage'>
				<span className='SpinnerPercentageValue'>{Math.floor(percentage)}</span>
				<span className='SpinnerPercentageSign'>%</span>
			</div>
		)
	}
}

const mapStateToProps = ({ spinner }) => ({ percentage : spinner.percentage || 0 })

SpinnerPercentage.propTypes = {
	percentage : PropTypes.number.isRequired
}
export default connect(mapStateToProps)(SpinnerPercentage)

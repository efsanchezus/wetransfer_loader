import React from 'react'
import { shallow } from 'enzyme'
import { SpinnerPercentage } from './SpinnerPercentage'

describe('SpinnerPercentage', () => {
	let wrapper;
	beforeEach(() => { 
		wrapper = shallow(
			<SpinnerPercentage 
				percentage={1}
			/>)
	})

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})
	it('should render a <span /> for the percentage value', () => {
		expect(wrapper.find('.SpinnerPercentageValue').length).toEqual(1)
	})
	it('should render a <span /> for the percentage sign', () => {
		expect(wrapper.find('.SpinnerPercentageSign').length).toEqual(1)
	})
	it('should render the value of the percentage', () => {
		expect(wrapper.text()).toEqual("1%")
		wrapper.setProps({ percentage : 40 })
		expect(wrapper.text()).toEqual("40%")
		wrapper.setProps({ percentage : 40.999 })
		expect(wrapper.text()).toEqual("40%")
		wrapper.setProps({ percentage : 101 })
		expect(wrapper.text()).toEqual("100%")
	})
})
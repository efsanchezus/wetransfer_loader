import React from 'react'
import { mount, shallow } from 'enzyme'
import { SpinnerCircle } from './SpinnerCircle'

describe('SpinnerCircle', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = shallow(
			<SpinnerCircle 
				width={100}
				percentage={1}
				inProcess={true}
				didSucceed={true}
			/>
		)
	})

	it('should render a <svg />', () => {
		expect(wrapper.find('svg').length).toEqual(1)
	})
	it('should render 1 <circle />s if didSucceed is true', () => {
		wrapper.setProps({ didSucceed : true });
		expect(wrapper.find('circle').length).toEqual(1)
	})
	it('should render 2 <circle />s if didSucceed is false', () => {
		wrapper.setProps({ didSucceed : false });
		expect(wrapper.find('circle').length).toEqual(2)
	})
})

describe('mounted SpinnerCircle', () => {
	let wrapper, props;
	beforeEach(() => {
		props = {
			width : 100,
			percentage : 1,
			inProcess: true,
			didSucceed: true
		}
		wrapper = mount(<SpinnerCircle {...props} />)
	})

	it('calls shouldRotate just when inProcess change its value', () => {
		const spy = jest.spyOn(wrapper.instance(), 'shouldRotate')
		wrapper.instance().forceUpdate();
		expect(spy).toHaveBeenCalledTimes(0)
		wrapper.setProps({ inProcess : false })
		expect(spy).toHaveBeenCalledTimes(1)
		wrapper.setProps({ inProcess : true })
		expect(spy).toHaveBeenCalledTimes(2)
		wrapper.setProps({ inProcess : true })
		expect(spy).toHaveBeenCalledTimes(2)
	})	

	it('should setState{ animate : true } when shouldRotate is called', () => {
		wrapper.setState({ animate : false });
		const spy = jest.spyOn(wrapper.instance(), 'shouldRotate')
		wrapper.instance().forceUpdate();
		wrapper.setProps({ inProcess : false })
		expect(wrapper.state('animate')).toEqual(true);
	})	
	it('should setState{ paused : false } when shouldRotate is called with parameter true', () => {
		wrapper.setState({ paused : false });
		const spy = jest.spyOn(wrapper.instance(), 'shouldRotate')
		wrapper.setProps({ inProcess : false })
		wrapper.setProps({ inProcess : true })
		expect(wrapper.state('paused')).toEqual(false);
	})	
	it('should setState{ paused : true } when shouldRotate is called with parameter false', () => {
		wrapper.setState({ paused : false });
		const spy = jest.spyOn(wrapper.instance(), 'shouldRotate')
		wrapper.setProps({ inProcess : true })
		wrapper.setProps({ inProcess : false })
		expect(wrapper.state('paused')).toEqual(true);
	})	

	it('should setState{ animate : false, paused : true } when didSucceed is true and updates its value', () => {
		wrapper.setState({ animate : true });
		wrapper.setState({ paused : false });

		wrapper.setProps({ didSucceed : false })
		wrapper.setProps({ didSucceed : true })

		expect(wrapper.state('animate')).toEqual(false);
		expect(wrapper.state('paused')).toEqual(false);
	})		
})







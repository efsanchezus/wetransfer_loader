import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import './SpinnerCircle.css' 

export class SpinnerCircle extends React.Component{
	constructor(){
		super()
		this.state = {
			animate : false,
			paused : false
		}
	}
	componentDidUpdate(prevProps){
		if(this.props.inProcess !== prevProps.inProcess){
			if(this.props.inProcess){
				this.shouldRotate(true)
			}else{
				this.shouldRotate(false)
			}
		}
		if(this.props.didSucceed !== prevProps.didSucceed && this.props.didSucceed === true){
			this.setState({
				animate : false,
				paused : false
			})				
		}
	}
	shouldRotate(bool){
		if(bool){
			this.setState({
				animate : true,
				paused : false
			})			
		}else{
			this.setState({
				animate : true,
				paused : true
			})					
		}
	}
	render(){
		const { animate, paused } = this.state
		const { width, percentage, didSucceed } = this.props

	    let strokeWidth = 8;
	    let center = width / 2;
	    let radius = (width - strokeWidth) / 2;
	    let circumference = 2* Math.PI * radius

	    let strokeDashoffset = circumference * (1 - (percentage / 100))

	    let classes = "SpinnerCircle"
	    if(animate){ classes += " SpinnerAnimation" }
	    if(paused){ classes += " SpinnerPaused" }

		return(
			<svg 
				className={classes}
				viewBox={`0 0 ${width} ${width}`}
				style={{  width, height: width }}>
	          <circle 
	            cx={center} 
	            cy={center}
	            r={radius} 
	            strokeWidth={strokeWidth}
	            stroke="#E7EBED"
	            fill="none"
	            strokeLinecap="round"
	            ></circle>		
	            {didSucceed === false 
	            	?
		          <circle 
		            cx={center} 
		            cy={center}
		            r={radius} 
		            strokeWidth={strokeWidth}
		            stroke="#3991EA"
		            fill="none"
		            strokeLinecap="round"
		            style={{ 
		              strokeDashoffset : strokeDashoffset,
		              strokeDasharray : circumference
		            }}
		            ></circle>	 
		            :
		            null
		        }           		
			</svg>
		)
	}
}

const mapStateToProps = ({ spinner }) => {
	return{
		width : spinner.width,
		percentage : spinner.percentage,
		inProcess : spinner.inProcess,
		didSucceed : spinner.didSucceed,
	}
}

SpinnerCircle.propTypes = {
	width : PropTypes.number.isRequired,
	percentage : PropTypes.number.isRequired,
	inProcess : PropTypes.bool.isRequired,
	didSucceed : PropTypes.bool.isRequired
}
export default connect(mapStateToProps)(SpinnerCircle)
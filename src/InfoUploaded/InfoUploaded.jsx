import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

export function InfoUploaded({ downloaded, fileSizeGB }){
	return(
		<span>{downloaded.toFixed(2)}GB of {fileSizeGB.toFixed(2)}GB uploaded</span>
	)
}

const mapStateToProps = ({ spinner }) => {
	const { fileSize, percentage } = spinner
	
	let fileSizeGB = fileSize / 1000
	let downloaded = percentage * fileSizeGB / 100 
	
	return{ fileSizeGB, downloaded }
}
InfoUploaded.propTypes = {
	downloaded : PropTypes.number.isRequired,
	fileSizeGB : PropTypes.number.isRequired
}
export default connect(mapStateToProps)(InfoUploaded)
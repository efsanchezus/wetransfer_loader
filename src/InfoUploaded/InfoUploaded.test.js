import React from 'react'
import { shallow } from 'enzyme'
import { InfoUploaded } from './InfoUploaded'

describe('InfoUploaded', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<InfoUploaded downloaded={50} fileSizeGB={100} />))

	it('should render a <span />', () => {
		expect(wrapper.find('span').length).toEqual(1)
	})
	it('should render a message containing the info about the progress of the transfer', () => {
		//{downloaded.toFixed(2)}GB of {fileSizeGB.toFixed(2)}GB uploaded
		expect(wrapper.find('span').text()).toEqual('50.00GB of 100.00GB uploaded')
		wrapper.setProps({ downloaded : 0, fileSizeGB : 100 })
		expect(wrapper.find('span').text()).toEqual('0.00GB of 100.00GB uploaded')
	})
})
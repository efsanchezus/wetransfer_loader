import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary'
import SpinnerCircle from '../SpinnerCircle/SpinnerCircle'
import SpinnerPercentage from '../SpinnerPercentage/SpinnerPercentage'
import GreenTick from '../GreenTick/GreenTick'
import './Spinner.css' 

export function Spinner({ didSucceed }){
	if(didSucceed === true){
		return(
			<div className='Spinner'>
				<ErrorBoundary text="">
					<SpinnerCircle />
					<GreenTick />
				</ErrorBoundary>
			</div>
		)
	}
	return(
		<div className='Spinner'>
			<ErrorBoundary text="">
				<SpinnerCircle />
			</ErrorBoundary>
			<ErrorBoundary text="">
				<SpinnerPercentage />
			</ErrorBoundary>
		</div>
	)
}

const mapStateToProps = ({ spinner }) => ({ didSucceed : spinner.didSucceed })

Spinner.propTypes = {
	didSucceed : PropTypes.bool.isRequired
}
export default connect(mapStateToProps)(Spinner)

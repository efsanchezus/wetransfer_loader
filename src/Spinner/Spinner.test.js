import React from 'react'
import { shallow } from 'enzyme'
import { Spinner } from './Spinner'
import SpinnerCircle from '../SpinnerCircle/SpinnerCircle'
import SpinnerPercentage from '../SpinnerPercentage/SpinnerPercentage'
import GreenTick from '../GreenTick/GreenTick'

describe('Spinner', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = shallow(
			<Spinner didSucceed={true} />
		)
	})

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})
	it('should render the SpinnerCircle and GreenTick if didSucceed is true', () => {
		expect(wrapper.containsAllMatchingElements([
			<SpinnerCircle />,
			<GreenTick />,
		])).toEqual(true)
	})

	it('should render the SpinnerCircle and SpinnerPercentage if didSucceed is false', () => {
		wrapper.setProps({ didSucceed : false })
		expect(wrapper.containsAllMatchingElements([
			<SpinnerCircle />,
			<SpinnerPercentage />,
		])).toEqual(true)
	})
	
})
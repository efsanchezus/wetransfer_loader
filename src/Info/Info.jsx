import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import InfoUploaded from '../InfoUploaded/InfoUploaded'
import TimeRemaining from '../TimeRemaining/TimeRemaining'
import './Info.css' 

export function Info({ didSucceed }){
	if(didSucceed === true){
		return(
			<div className='Info'>You will shortly receive a confirmation email</div>
		)
	}
	return(
		<div className='Info'>
			<span>Sending <a href='#'>11 files to 4 recipients</a></span>
			<InfoUploaded />
			<TimeRemaining />
		</div>
	)
}

const mapStateToProps = ({ spinner }) => ({ didSucceed : spinner.didSucceed })
Info.propTypes = {
	didSucceed : PropTypes.bool.isRequired
}
export default connect(mapStateToProps)(Info)
import React from 'react'
import { shallow } from 'enzyme'
import { Info } from './Info'
import InfoUploaded from '../InfoUploaded/InfoUploaded'
import TimeRemaining from '../TimeRemaining/TimeRemaining'

describe('Info', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<Info didSucceed={false} />))

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})
	it('should render the InfoUploaded, TimeRemaining and a <span />', () => {
		expect(wrapper.find('span').length).toEqual(1)
		expect(wrapper.containsAllMatchingElements([
			<InfoUploaded />,
			<TimeRemaining />
		]))
	})
	it('should render a <p /> if didSucceed is true', () => {
		wrapper.setProps({ didSucceed : true })
		expect(wrapper.find('div').length).toEqual(1)
	})
})
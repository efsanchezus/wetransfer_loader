import React from 'react'
import { shallow } from 'enzyme'
import { TimeRemaining } from './TimeRemaining'

describe('TimeRemaining', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<TimeRemaining timeRemaining="" />))

	it('should render a <span />', () => {
		expect(wrapper.find('span').length).toEqual(1)
	})
	it('should render a the time remaining', () => {
		wrapper.setProps({ timeRemaining : '4 seconds' })
		expect(wrapper.find('span').text()).toEqual('4 seconds remaining')
	})
})
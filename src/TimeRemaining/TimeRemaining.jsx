import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

export function TimeRemaining({ timeRemaining }){
	return(<span>{timeRemaining} remaining</span>)
}

const mapStateToProps = ({ spinner }) => {
	const { fileSize, percentage, speed } = spinner
	
	let fileSizeGB = fileSize / 1000
	let downloaded = percentage * fileSizeGB / 100 

	//speed -> (1/100) gb / ms
	let totalTime = fileSizeGB * speed * 100
	let estimatedTime = totalTime - downloaded * speed * 100

	let totalTimeInSeconds = estimatedTime / 1000
	let toMinutes = Math.floor(totalTimeInSeconds / 60)
	let toSeconds = (totalTimeInSeconds % 60).toFixed(0)

	let timeRemaining = ""
	if(toMinutes < 1){
		timeRemaining = " " + toSeconds + " seconds"
	}else if(toMinutes === 1){
		timeRemaining = " " + toMinutes + " minute"
	}else if(toMinutes >= 1){
		timeRemaining = " " + toMinutes + " minutes"
	}

	return{ timeRemaining }
}
TimeRemaining.propTypes = {
	timeRemaining : PropTypes.string.isRequired
}
export default connect(mapStateToProps)(TimeRemaining)
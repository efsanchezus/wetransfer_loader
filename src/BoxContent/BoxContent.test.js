import React from 'react'
import { shallow } from 'enzyme'
import BoxContent from './BoxContent'
import Spinner from '../Spinner/Spinner'
import TransferStatus from '../TransferStatus/TransferStatus'
import Info from '../Info/Info'

describe('BoxContent', () => {
	let wrapper;
	beforeEach(() => wrapper = shallow(<BoxContent />))

	it('should render a <div />', () => {
		expect(wrapper.find('div').length).toEqual(1)
	})

	it('should render the Spinner, TransferStatus, Info', () => {
		expect(wrapper.containsAllMatchingElements([
			<Spinner />,
			<TransferStatus />,
			<Info />
		])).toEqual(true)
	})
})
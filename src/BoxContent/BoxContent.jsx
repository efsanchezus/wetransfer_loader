import React from 'react'
import Spinner from '../Spinner/Spinner'
import TransferStatus from '../TransferStatus/TransferStatus'
import Info from '../Info/Info'
import './BoxContent.css' 

function BoxContent(){
	return(
		<div className='BoxContent'>
			<Spinner />
			<TransferStatus />
			<Info />
		</div>
	)
}
export default BoxContent